﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class canvasLogos : MonoBehaviour {

    public string levelName;
    private GameManager gameManager;

	// Use this for initialization
	void Start () {
        Invoke("forward", 3.0f);
        if (GameManager.instance == null)
            Instantiate(gameManager);
	}

    void forward()
    {
        SceneManager.LoadScene(levelName);
    }
}
