﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SelectCharacterSceneManager : MonoBehaviour
{

    public Button eletricista;
    public Button buttonOption;

    public Dropdown alternativas;
    public Text descricaoManutencao;

    public Transform scrollList;
    public Transform respostasList;

    public List<Sprite> fotosEletricistas;

    private List<string> passosDaManutencao;

    private GameObject SpawnPoint;

    private int timeDeManutencao;

    // Use this for initialization
    void Start()
    {
        SpawnPoint = GameObject.FindGameObjectWithTag("Spawn");
        passosDaManutencao = GameManager.getPassosForActivity();
        descricaoManutencao.text = GameManager.currentMaintenance;
        timeDeManutencao = 0;

        while(passosDaManutencao.Count > 0)
        {
            int posicaoItem = Random.Range(0, passosDaManutencao.Count - 1);
            alternativas.options.Add(new Dropdown.OptionData(passosDaManutencao.ElementAt<string>(posicaoItem)));
            passosDaManutencao.RemoveAt(posicaoItem);
        }

        alternativas.RefreshShownValue();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClickAddElement()
    {
        Button newEletricista = Instantiate(eletricista) as Button;
        newEletricista.image.sprite = fotosEletricistas[Random.Range(0, 2)];
        newEletricista.transform.SetParent(scrollList);
        timeDeManutencao++;
    }

    public void OnClickUnload()
    {
        if (timeDeManutencao > 0)
        {
            ExecuteEvents.Execute<IEletricistaMessages>(SpawnPoint,
                null,
                (x, y) => x.callMaintenance(GameManager.RepairPoint));
        }
  
        SceneManager.UnloadScene("Titulo");
    }

    public void OnClickAddAlternativa()
    {
        Button optionAdded = Instantiate(buttonOption) as Button;
        optionAdded.GetComponentInChildren<Text>().text = alternativas.captionText.text;
       // alternativas.
        alternativas.options.RemoveAt(alternativas.value);
        alternativas.RefreshShownValue();
        optionAdded.transform.SetParent(respostasList);
    }
}
