﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    private static int teste = 0;

    private static XDocument xmlAtividades;
    public string xmlFileName;

    private IEnumerable<XElement> atividadesDeManutencao;

    private static List<string> vetorDeAtividades;

    public static GameObject RepairPoint;
    public static string currentMaintenance;

    // Use this for initialization
    void Awake () {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
	}

    void Start()
    {
        vetorDeAtividades = new List<string>();

        xmlAtividades = XDocument.Load(xmlFileName);

        atividadesDeManutencao = from item in xmlAtividades.Root.Descendants("Atividade")
                                 select item;

        foreach (XElement item in atividadesDeManutencao)
        {
            vetorDeAtividades.Add( item.FirstNode.ToString());
        }
    }

    public static List<string> getPassosForActivity(string activity)
    {
        List<string> toReturn = new List<string>();
        XElement searchDomain = xmlAtividades.Root;

        IEnumerable<IEnumerable<XElement>> passosAtividade = from item in searchDomain.Elements("Atividade")
                                       where item.FirstNode.ToString() == activity
                                       select item.Elements("Passo");

        foreach (IEnumerable<XElement> item in passosAtividade)
            foreach (XElement passo in item)
                toReturn.Add(passo.Value);

        return toReturn;
    }

    public static string getRandomActivity()
    {
        return vetorDeAtividades.ElementAt<string>(Random.Range(0, vetorDeAtividades.Count() - 1)); 
    }

    public static List<string> getPassosForActivity()
    {
        return getPassosForActivity(currentMaintenance);
    }

}
