﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class IncidentManager : MonoBehaviour {

    private GameObject[] breakableObjectsInScene;

    // Use this for initialization
    void Start () {
        //  Initializes the array with all the breakable objects
        //  all breakable objects in the scene have the tag "breakable"
        breakableObjectsInScene = GameObject.FindGameObjectsWithTag("breakable");
    }
	
	// Update is called once per frame
	void Update () {

        if ( Input.GetKeyDown("q") )
        {
            int objectToBreak = Random.Range(0, breakableObjectsInScene.Length - 1);

            ExecuteEvents.Execute<IMessageBreak>(breakableObjectsInScene[objectToBreak], null, (x, y)=>x.blow() );
        }
        
	}

    public void SairDoJogo()
    {
        Application.Quit();
    }
}
