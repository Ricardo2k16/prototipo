﻿using UnityEngine;
using System.Collections;
using System;

public class Eletricista : MonoBehaviour, IEletricistaMessages {

    private GameObject spawn;
    private bool maintenanceFinished;

    public void callMaintenance(GameObject broken)
    {
        throw new NotImplementedException();
    }

    public void maintenanceEnded()
    {
        Debug.Log("Manutenção terminada");

        maintenanceFinished = true;
        NavMeshAgent navmeshAgent = FindObjectOfType<NavMeshAgent>();
        navmeshAgent.destination = spawn.transform.position;
        navmeshAgent.Resume();
    }

    // Use this for initialization
    void Start () {

        spawn = GameObject.FindGameObjectWithTag("Spawn");
        maintenanceFinished = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Spawn" && maintenanceFinished)
        {
            Destroy(gameObject);
        }
    }
}
