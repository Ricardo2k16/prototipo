﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public interface IEletricistaMessages : IEventSystemHandler {

    //  Message sent to create an electritian
    void callMaintenance(GameObject broken);

    //  Message sent to eletricsta to come back after finishing maintenance
    void maintenanceEnded();

}
