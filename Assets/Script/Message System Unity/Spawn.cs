﻿using UnityEngine;
using System.Collections;
using System;

public class Spawn : MonoBehaviour, IEletricistaMessages {

    public GameObject eletricista;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void callMaintenance(GameObject broken)
    {
        GameObject instantiated = Instantiate( eletricista, 
            gameObject.transform.position,
            Quaternion.LookRotation(broken.transform.position) ) as GameObject;

        instantiated.GetComponent<NavMeshAgent>().destination = broken.gameObject.transform.position;
    }

    public void maintenanceEnded()
    {
        throw new NotImplementedException();
    }
}
