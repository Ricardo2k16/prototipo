﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public interface IMessageBreak : IEventSystemHandler
{
    //  Message to enable malfunctioning
    void blow();

    //  Message sent to disable malfunctioning
    void mend();
}