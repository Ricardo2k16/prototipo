﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class Break : MonoBehaviour, IMessageBreak {

    private ParticleSystem Smoke;
    private GameObject SpawnPoint;
    private GameObject RepairPoint;

    private GameObject collided;

    public int toMend = 10;

    [HideInInspector] public bool isBroken;

    [HideInInspector] public string problem;
    
    // Use this for initialization
    void Start () {
        Smoke = GetComponentInChildren<ParticleSystem>();
        SpawnPoint = GameObject.FindGameObjectWithTag("Spawn");
        RepairPoint = transform.Find("MendingPoint").gameObject;

        isBroken = false;
	}

    //Interface implementation
    public void blow()
    {
        isBroken = true;
        GameManager.RepairPoint = RepairPoint;
        problem = GameManager.getRandomActivity();
        Smoke.Play();
    }

    public void mend()
    {
        isBroken = false;
        Smoke.Stop();
        ExecuteEvents.Execute<IEletricistaMessages>(collided,
            null,
            (x, y) => x.maintenanceEnded());
    }

    void OnMouseDown()
    {
        if (isBroken)
        {
            GameManager.currentMaintenance = problem;
            SceneManager.LoadScene("Titulo", LoadSceneMode.Additive);
        }
    }

    void OnCollisionEnter (Collision collision)
    {
        collided = collision.gameObject;

        if(collided.tag == "Eletricista")
        {
            collided.GetComponent<NavMeshAgent>().Stop();
            StartCoroutine(fixActivity());
        }
    }

    IEnumerator fixActivity()
    {
        int dano = 100;

        while (dano > 0)
        {
            dano-=toMend;
            yield return new WaitForSeconds(1f);
        }

        mend();
        
    }
}